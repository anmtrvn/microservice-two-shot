from django.shortcuts import render
from .models import HatList
from .models import HatLocation
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder


# Create your views here.


class HatLocationEncoder(ModelEncoder):
    model = HatLocation
    properties = [
        'closet_name',
        'section_number',
        'shelf_number',
        'import_href',
    ]

class HatListEncoder(ModelEncoder):
    model = HatList
    properties = [
        'id',
        'fabric',
        'style_name',
        'color',
        'url',
        'location',
    ]
    encoders = {
        'location': HatLocationEncoder(),
    }

@require_http_methods(["GET", "POST", "DELETE"])
def hats_view(request, hat_id=None):
    if request.method == "GET":
        hatList = HatList.objects.all()
        return JsonResponse(
            {"hatList": hatList},
            encoder=HatListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            hatLocation_address = content["location"]
            hatLocation = HatLocation.objects.get(import_href=hatLocation_address)
            content["location"] = hatLocation
        except hatLocation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hatList = HatList.objects.create(**content)
        return JsonResponse(
            hatList,
            encoder=HatListEncoder,
            safe=False,
        )
    else:
        try:
            hat = HatList.objects.get(pk=hat_id)
            hat.delete()
            return JsonResponse({"message": "Hat deleted successfully!"})
        except HatList.DoesNotExist:
            return JsonResponse({"message": "Hat not found!"})
